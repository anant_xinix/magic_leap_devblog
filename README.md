Magic Dev blog

* Uses firebase for real time data storage, updates and authentication.
* Used a react-boilerplate to create the basic template along with a pre-configured build tool.
* The react-boilerplate generator can be found at: https://github.com/facebookincubator/create-react-app
* Used redux for state management, reactjs as the front-end framework and react-bootstrap for the UI components.
* The UI is not completely refined in the sense that I was mostly trying to build the entire app by only using the components from react-bootstrap with as little custom styling as possible.
* Rules mentioned in the requirements are enforced through firebase database rules. They can be previewed in `database.rules.json`

Reasons for Delay

* At Groupon we are banned from using ReactJS, hence took some extra time to figure out the best practices and application design.
* Have been experiencing heavy workloads because I have been leading a major refactoring effort in my latest project.
* I was suddenly bombarded with multiple take home assignments and interviews over the past two weeks.