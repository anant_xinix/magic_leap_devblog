import React from 'react';
import { Panel } from 'react-bootstrap';
import './index.css';

function Comment(props) {
  const footer = (<span>Posted by {props.author} on {props.time}</span>);
  return (
    <Panel footer={footer} className="comment">
      {props.comment}
    </Panel>
  );
}

Comment.propTypes = {
  comment: React.PropTypes.string,
  author: React.PropTypes.string,
  time: React.PropTypes.string,
};

Comment.defaultProps = {
  comment: '',
  author: '',
  time: '',
};

export default Comment;
