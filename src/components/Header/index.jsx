import React from 'react';
import './index.css';
import logo from './logo.svg';

function Header(props) {
  return (
    <header>
      <img src={logo} className="logo" alt="logo" />
      <h2>{props.title}</h2>
    </header>
  );
}

Header.propTypes = {
  title: React.PropTypes.string,
};

Header.defaultProps = {
  title: 'Magic Leap Developer Blog',
};

export default Header;
