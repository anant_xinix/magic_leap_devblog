import React, { Component } from 'react';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router';

class Navigation extends Component {

  showUserPanel() {
    return [
      <Navbar.Text pullRight key="logout">
        <Link to="/logout">Logout</Link>
      </Navbar.Text>,
      <Navbar.Text pullRight key="greet">
        Welcome {this.props.user.displayName}
      </Navbar.Text>,
    ];
  }

  showLoginPanel() {
    return [
      <Navbar.Text pullRight key="login">
        <Link to="/login">Login</Link>
      </Navbar.Text>,
      <Navbar.Text pullRight key="signup">
        <Link to="/signup">Sign Up</Link>
      </Navbar.Text>,
    ];
  }

  render() {
    return (
      <Navbar fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Magic Leap</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Navbar.Text>
            <Link to="/blog/edit">Create a Blog Post</Link>
          </Navbar.Text>
          {this.props.user !== null ? this.showUserPanel() : this.showLoginPanel()}
        </Navbar.Collapse>
      </Navbar>
    );
  }

}

Navigation.propTypes = {
  user: React.PropTypes.object,
};

Navigation.defaultProps = {
  user: null,
};

export default Navigation;
