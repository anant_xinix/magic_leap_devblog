import React from 'react';
import { Modal, Alert, Button } from 'react-bootstrap';

function Notification(props) {
  if (props.alert) {
    return (
      <Modal show={props.show} onDismiss={props.onDismiss}>
        <Modal.Body>
          <Alert bsStyle={props.alert.type}>{props.alert.msg}</Alert>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" onClick={() => props.onDismiss()}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  return (<Modal />);
}

Notification.propTypes = {
  show: React.PropTypes.bool,
  onDismiss: React.PropTypes.func,
  alert: React.PropTypes.shape({
    title: React.PropTypes.string,
    type: React.PropTypes.string,
    msg: React.PropTypes.string,
  }),
};

Notification.defaultProps = {
  show: false,
  onDismiss: null,
  alert: null,
};

export default Notification;
