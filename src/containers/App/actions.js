import { LOGIN_USER, LOGOUT_USER, SHOW_ALERT, DISMISS_ALERT } from './constants';

export function logoutUser() {
  return {
    type: LOGOUT_USER,
  };
}

export function loginUser(user) {
  return {
    type: LOGIN_USER,
    user,
  };
}

export function showAlert(alert) {
  return {
    type: SHOW_ALERT,
    alert,
  };
}

export function dismissAlert() {
  return {
    type: DISMISS_ALERT,
  };
}
