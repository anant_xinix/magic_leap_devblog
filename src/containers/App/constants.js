export const LOGOUT_USER = 'app/LOGOUT_USER';
export const LOGIN_USER = 'app/LOGIN_USER';
export const SHOW_ALERT = 'app/SHOW_ALERT';
export const DISMISS_ALERT = 'app/DISMISS_ALERT';
