import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Header from '../../components/Header';
import Navigation from '../../components/Navigation';
import Notification from '../../components/Notification';
import './index.css';
import { selectUser, selectLocationState, selectAlert } from './selectors';
import { dismissAlert } from './actions';

function App(props) {
  return (
    <div className="App">
      <Helmet
        defaultTitle="Magic Leap Dev"
        titleTemplate="% - Magic Leap Dev"
      />
      <Navigation user={props.user} />
      <Header />
      <Notification
        onDismiss={props.dispatchDismissAlert}
        show={props.alert !== null}
        alert={props.alert}
      />
      <Grid>
        <Row>
          <Col lgOffset={2} lg={8} md={10} mdOffset={1}>
            { React.Children.toArray(props.children) }
          </Col>
        </Row>
      </Grid>
    </div>
  );
}

App.propTypes = {
  children: React.PropTypes.node,
  dispatchDismissAlert: React.PropTypes.func,
  alert: React.PropTypes.shape({
    type: React.PropTypes.string,
    msg: React.PropTypes.string,
  }),
  user: React.PropTypes.object,
};

App.defaultProps = {
  children: null,
  dispatchDismissAlert: null,
  alert: null,
  user: null,
};

const mapStateToProps = createStructuredSelector({
  user: selectUser(),
  location: selectLocationState(),
  alert: selectAlert(),
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatchDismissAlert: () => {
      dispatch(dismissAlert());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
