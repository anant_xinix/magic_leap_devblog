import { fromJS } from 'immutable';
import { LOGIN_USER, LOGOUT_USER, SHOW_ALERT, DISMISS_ALERT } from './constants';

const initialState = fromJS({
  user: null,
  alert: null,
});

function globalReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER:
      return state
        .set('user', action.user);
    case LOGOUT_USER:
      return state
        .set('user', null);
    case SHOW_ALERT:
      return state
        .set('alert', action.alert);
    case DISMISS_ALERT:
      return state
        .set('alert', null);
    default:
      return state;
  }
}

export default globalReducer;
