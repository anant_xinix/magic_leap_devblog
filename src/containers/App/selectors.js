import { createSelector } from 'reselect';

const selectGlobal = () => state => state.get('global');

const selectUser = () => createSelector(
  selectGlobal(),
  globalState => globalState.get('user'),
);

const selectAlert = () => createSelector(
  selectGlobal(),
  globalState => globalState.get('alert'),
);

const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectGlobal,
  selectUser,
  selectAlert,
  selectLocationState,
};
