import React, { Component } from 'react';
import { Panel, FormControl, Form, FormGroup, ControlLabel, Col, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { createStructuredSelector } from 'reselect';
import { stateToHTML } from 'draft-js-export-html';
import { stateFromHTML } from 'draft-js-import-html';
import firebase from 'firebase';
import { EditorState } from 'draft-js';
import { browserHistory } from 'react-router';
import { selectUser } from '../App/selectors';
import './index.css';
import { showAlert } from '../App/actions';

class BlogEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: null,
      isLoading: false,
    };
  }

  componentDidMount() {
    if (!this.props.user) {
      this.props.dispatchShowAlert({
        type: 'danger',
        msg: 'You need to be logged in to create or edit a post',
      });
    }

    if (this.props.params.id) {
      const blog = firebase.database().ref(`blogs/${this.props.params.id}`);
      blog.on('value', this.blogLoad.bind(this));
    }
  }

  onEditorStateChange(editorState) {
    this.setState({
      editorState,
    });
  }

  onSaveBlog(e) {
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    const time = new Date();
    this.setState({ isLoading: true });
    const blog = {
      title: this.titleInp.value,
      content: stateToHTML(this.state.editorState.getCurrentContent()),
      username: this.props.user.displayName,
      datetime: time.toISOString(),
      uid: this.props.user.uid,
    };
    if (this.props.params.id) {
      this.props.dispatchUpdateBlog(this.props.params.id, blog, () => {
        this.setState({ isLoading: false });
      });
    } else {
      this.props.dispatchSaveBlog(blog, () => {
        this.setState({ isLoading: false });
      });
    }
  }

  blogLoad(blog) {
    if (!blog) {
      return;
    }
    this.titleInp.value = blog.val().title;
    this.setState({
      editorState: EditorState.createWithContent(
        stateFromHTML(blog.val().content)),
    });
  }

  render() {
    return (
      <div>
        <Panel>
          <Form horizontal>
            <FormGroup>
              <Col sm={2} componentClass={ControlLabel}>
                Title
              </Col>
              <Col sm={10}>
                <FormControl
                  type="title"
                  placeholder="Enter a title for the blog"
                  required
                  inputRef={(ref) => {
                    this.titleInp = ref;
                  }}
                  onChange={e => this.setState({ title: e.target.value })}
                />
              </Col>
            </FormGroup>
          </Form>
        </Panel>
        <Panel>
          <Editor
            editorState={this.state.editorState}
            toolbarClassName="home-toolbar"
            wrapperClassName="home-wrapper"
            editorClassName="home-editor"
            onEditorStateChange={e => this.onEditorStateChange(e)}
          />
        </Panel>
        <Panel>
          <Button bsStyle="primary" disabled={this.state.isLoading} onClick={e => this.onSaveBlog(e)}>
            {this.state.isLoading ? 'Saving....' : 'Save'}
          </Button>
        </Panel>
      </div>
    );
  }
}

BlogEditor.propTypes = {
  user: React.PropTypes.object,
  params: React.PropTypes.shape({
    id: React.PropTypes.string,
  }),
  dispatchSaveBlog: React.PropTypes.func,
  dispatchUpdateBlog: React.PropTypes.func,
  dispatchShowAlert: React.PropTypes.func,
};

BlogEditor.defaultProps = {
  user: null,
  params: null,
  dispatchSaveBlog: null,
  dispatchUpdateBlog: null,
  dispatchShowAlert: null,
};

const mapStatetoProps = createStructuredSelector({
  user: selectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatchUpdateBlog: (key, blog, cb) => {
      const database = firebase.database();
      database.ref(`blogs/${key}`).set(blog).then(() => {
        dispatch(showAlert({
          type: 'success',
          msg: 'Blog Saved',
        }));
        cb();
      });
    },
    dispatchSaveBlog: (blog, cb) => {
      const database = firebase.database();
      database.ref('blogs/').push(blog).then((data) => {
        browserHistory.push(`/blog/edit/${data.key}`);
        dispatch(showAlert({
          type: 'success',
          msg: 'Blog Saved',
        }));
        cb();
      });
    },
    dispatchShowAlert: (alert) => {
      browserHistory.push('/login');
      dispatch(showAlert(alert));
    },
  };
}

export default connect(mapStatetoProps, mapDispatchToProps)(BlogEditor);
