import React, { Component } from 'react';
import { Form, FormGroup, FormControl, Col, Button, PageHeader, Modal } from 'react-bootstrap';
import firebase from 'firebase';
import renderHTML from 'react-render-html';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Comment from '../../components/Comment';
import './index.css';
import { showAlert } from '../App/actions';
import { selectUser } from '../App/selectors';

class BlogPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blog: null,
      comments: [],
      showPrompt: false,
    };
  }

  componentDidMount() {
    const blogRef = firebase.database().ref(`blogs/${this.props.params.id}`);
    const commentRef = firebase.database().ref('comments/');
    commentRef.orderByChild('post_id').equalTo(this.props.params.id);

    blogRef.on('value', this.onBlogLoad.bind(this));
    commentRef.on('child_added', this.onLoadComment.bind(this));
  }

  componentWillUnmount() {
    const blogRef = firebase.database().ref(`blogs/${this.props.params.id}`);
    blogRef.off('value', this.onBlogLoad.bind(this));
    const commentRef = firebase.database().ref('comments/');
    commentRef.off('child_added', this.onLoadComment.bind(this));
  }

  onBlogLoad(data) {
    this.setState({ blog: data });
  }

  onLoadComment(comment) {
    this.setState((prevState) => {
      const arr = prevState.comments;
      arr.push(comment);
      return {
        comments: arr,
      };
    });
  }

  submitComment(e) {
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    if (!this.props.user) {
      browserHistory.push('/login');
      this.props.dispatchShowAlert({
        type: 'danger',
        msg: 'You need to be logged in to post a comment',
      });
      return;
    }
    const commentRef = firebase.database().ref('comments/').push();
    const time = new Date();
    commentRef.set({
      post_id: this.state.blog.key,
      content: e.target.commentBox.value,
      username: this.props.user.displayName,
      datetime: time.toISOString(),
    });
  }

  showModifiers() {
    if (this.props.user && this.state.blog.val().username === this.props.user.displayName) {
      return [
        <Button
          bsStyle="danger" className="btn-delete" key="del"
          onClick={() => this.setState({ showPrompt: true })}
        >Delete</Button>,
        <Button
          bsStyle="info" className="btn-edit" key="edit"
          onClick={() => {
            this.setState({ showPrompt: false });
            browserHistory.push(`/blog/edit/${this.props.params.id}`);
          }}
        >Edit</Button>,
      ];
    }
    return '';
  }

  deleteBlog() {
    const blogRef = firebase.database().ref(`blogs/${this.props.params.id}`);
    browserHistory.push('/');
    blogRef.remove().then(() => {
      this.props.dispatchShowAlert({
        type: 'info',
        msg: 'The blog has been deleted',
      });
    }).catch((error) => {
      this.props.dispatchShowAlert({
        type: 'dander',
        msg: error.message,
      });
    });
  }

  render() {
    if (this.state.blog) {
      const blog = this.state.blog.val();
      const getDate = date => new Date(date);
      return (
        <div className="blog-content">
          <Modal show={this.state.showPrompt}>
            <Modal.Body>
              Are you sure you want to delete this post?
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.deleteBlog()}>Yes</Button>
              <Button onClick={() => this.setState({ showPrompt: false })}>No</Button>
            </Modal.Footer>
          </Modal>
          <PageHeader>
            {blog.title}<br />
            <small>Posted by {blog.author} at {getDate(blog.datetime).toDateString()}</small>
            {this.showModifiers()}
          </PageHeader>
          {renderHTML(blog.content)}
          <PageHeader className="comment-header">
            Comments
          </PageHeader>
          {this.state.comments.map(comment => (
            <Comment
              key={comment.key}
              author={comment.val().username}
              comment={comment.val().content}
              time={getDate(comment.val().datetime).toDateString()}
            />
          ))}
          <Form horizontal onSubmit={e => this.submitComment(e)}>
            <FormGroup controlId="formCommentBox">
              <Col sm={10}>
                <FormControl name="commentBox" componentClass="textarea" placeholder="Post a comment"/>
              </Col>
            </FormGroup>
            <Button type="submit">
              Submit
            </Button>
          </Form>

        </div>
      );
    }
    return (
      <div />
    );
  }
}

BlogPost.propTypes = {
  params: React.PropTypes.shape({
    id: React.PropTypes.string,
  }),
  user: React.PropTypes.object,
  dispatchShowAlert: React.PropTypes.func,
};

BlogPost.defaultProps = {
  params: {
    id: '',
  },
  user: null,
  dispatchShowAlert: null,
};

const mapStateToProps = createStructuredSelector({
  user: selectUser(),
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatchShowAlert: (alert) => {
      dispatch(showAlert(alert));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BlogPost);
