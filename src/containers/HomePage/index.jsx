import React, { Component } from 'react';
import firebase from 'firebase';
import { Link } from 'react-router';
import { PageHeader } from 'react-bootstrap';
import './index.css';

class HomePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startAt: 0,
      limit: 5,
      blogs: [],
    };
  }

  componentDidMount() {
    const blogsRef = firebase.database().ref('blogs/');

    blogsRef.on('child_added', this.onChildAdded.bind(this));
  }

  componentWillUnmount() {
    const blogsRef = firebase.database().ref('blogs/');

    blogsRef.off('child_added', this.onChildAdded.bind(this));
  }

  onChildAdded(data) {
    this.setState((prevState) => {
      const arr = prevState.blogs;
      arr.push(data);
      return {
        blogs: arr,
      };
    });
  }

  render() {
    const getDate = date => new Date(date);
    return (
      <div>
        {this.state.blogs.map(blog => (
          <PageHeader key={blog.key}>
            <Link to={`/blog/${blog.key}`}>
              {blog.val().title}<br />
              <small>
                Posted by {blog.val().username} on {getDate(blog.val().datetime).toDateString()}
              </small>
            </Link>
          </PageHeader>
        ))}
      </div>);
  }
}

export default HomePage;
