import React, { Component } from 'react';
import { Row, Col, Panel, Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import firebase from 'firebase';
import { connect } from 'react-redux';
import './index.css';
import { showAlert } from '../App/actions';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  login(evt) {
    if (evt && evt.preventDefault) {
      evt.preventDefault();
    }
    this.setState({ isLoading: true });
    const email = evt.target.emailId.value;
    const password = evt.target.password.value;

    firebase.auth().signInWithEmailAndPassword(email, password).catch((error) => {
      this.props.dispatchShowAlert({
        alertMsg: error.message,
        alertType: 'danger',
      });
      this.setState({
        isLoading: false,
      });
    });
  }

  render() {
    return (
      <Row>
        <Col sm={8} smOffset={2}>
          <Panel header="Login">
            <Form horizontal onSubmit={e => this.login(e)}>
              <FormGroup controlId="formEmail">
                <Col componentClass={ControlLabel} sm={2}>
                  Email
                </Col>
                <Col sm={10}>
                  <FormControl type="email" placeholder="Email" name="emailId" />
                </Col>
              </FormGroup>
              <FormGroup controlId="formPassword">
                <Col componentClass={ControlLabel} sm={2}>
                  Password
                </Col>
                <Col sm={10}>
                  <FormControl type="password" placeholder="Enter a password" name="password" />
                </Col>
              </FormGroup>
              <FormGroup>
                <Col smOffset={8} sm={4}>
                  <Button bsStyle="primary" disabled={this.state.isLoading} type="submit">
                    {this.state.isLoading ? 'Loading....' : 'Log In'}
                  </Button>
                </Col>
              </FormGroup>
            </Form>
          </Panel>
        </Col>
      </Row>
    );
  }
}

Login.propTypes = {
  dispatchShowAlert: React.PropTypes.func,
};

Login.defaultProps = {
  dispatchShowAlert: null,
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchShowAlert: (alert) => {
      dispatch(showAlert(alert));
    },
  };
}

export default connect(null, mapDispatchToProps)(Login);
