import React, { Component } from 'react';
import { Row, Col, Panel, Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import firebase from 'firebase';
import './index.css';
import { showAlert } from '../App/actions';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: '',
      rePassword: '',
      isValidPassword: false,
    };
  }

  signUp(evt) {
    if (evt && evt.preventDefault) {
      evt.preventDefault();
    }
    if (this.state.isValidPassword) {
      this.props.dispatchUserRegistration({
        emailId: evt.target.emailId.value,
        password: evt.target.password.value,
        username: evt.target.username.value,
      });
    }
  }

  handleAlertDismiss() {
    this.setState({ alertVisible: false });
  }

  updatePassword(evt) {
    this.setState({ password: evt.target.value });
  }

  checkPassword(evt) {
    const password = evt.target.value;
    if (password && password.length >= 8 && password === this.state.password) {
      this.setState({ isValidPassword: true });
    } else {
      this.setState({ isValidPassword: false });
    }
  }

  render() {
    return (
      <Row>
        <Col sm={8} smOffset={2}>
          <Panel header="Sign Up">
            <Form horizontal onSubmit={e => this.signUp(e)}>
              <FormGroup controlId="formEmail">
                <Col componentClass={ControlLabel} sm={2}>
                  Email
                </Col>
                <Col sm={10}>
                  <FormControl type="email" name="emailId" placeholder="Email" required/>
                </Col>
              </FormGroup>
              <FormGroup controlId="formUsername">
                <Col componentClass={ControlLabel} sm={2}>
                  Username
                </Col>
                <Col sm={10}>
                  <FormControl type="text" name="username" placeholder="Username" required/>
                </Col>
              </FormGroup>
              <FormGroup
                controlId="formPassword"
                validationState={this.state.isValidPassword ? 'success' : 'warning'}
              >
                <Col componentClass={ControlLabel} sm={2}>
                  Password
                </Col>
                <Col sm={10}>
                  <FormControl
                    type="password"
                    name="password"
                    value={this.state.password}
                    placeholder="Enter a password"
                    required
                    onChange={e => this.updatePassword(e)}
                  />
                </Col>
              </FormGroup>
              <FormGroup
                controlId="formRePassword"
                validationState={this.state.isValidPassword ? 'success' : 'warning'}
              >
                <Col componentClass={ControlLabel} sm={2}>
                  Re-enter Password
                </Col>
                <Col sm={10}>
                  <FormControl
                    type="password"
                    name="rePassword"
                    placeholder="Enter your password again"
                    onChange={e => this.checkPassword(e)}
                  />
                </Col>
              </FormGroup>
              <FormGroup>
                <Col smOffset={8} sm={4}>
                  <Button bsStyle="primary" type="submit">Sign Up</Button>
                </Col>
              </FormGroup>
            </Form>
          </Panel>
        </Col>
      </Row>
    );
  }
}

SignUp.propTypes = {
  dispatchUserRegistration: React.PropTypes.func,
};

SignUp.defaultProps = {
  dispatchUserRegistration: null,
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchUserRegistration: (userInfo) => {
      dispatch(showAlert({
        type: 'info',
        msg: 'Signing Up! Please Wait',
      }));
      firebase.auth().createUserWithEmailAndPassword(userInfo.emailId, userInfo.password)
        .then((user) => {
          user.updateProfile({
            displayName: userInfo.username,
          });
        })
        .catch((error) => {
          dispatch(showAlert({
            type: 'danger',
            msg: error.message,
          }));
        });
    },
  };
}

export default connect(null, mapDispatchToProps)(SignUp);
