import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory, Route, IndexRoute } from 'react-router';
import { Alert } from 'react-bootstrap';
import { Provider } from 'react-redux';
import firebase from 'firebase';
import configureStore from './store/store';
import HomePage from './containers/HomePage';
import SignUp from './containers/SignUp';
import Login from './containers/Login';
import BlogPost from './containers/BlogPost';
import BlogEditor from './containers/BlogEditor';
import App from './containers/App';
import './index.css';
import { logoutUser, loginUser } from './containers/App/actions';

const initialState = {};

let preLoading = true;

const store = configureStore(initialState, history);

const config = {
  apiKey: "AIzaSyABarQWZE31Ne5ZLeAIEjOUQl_ezyFCNbA",
  authDomain: "magic-devblog.firebaseapp.com",
  databaseURL: "https://magic-devblog.firebaseio.com",
  storageBucket: "magic-devblog.appspot.com",
  messagingSenderId: "604613986395"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged((user) => {
  if(user){
    store.dispatch(loginUser(user));
  }
  if(preLoading) {
    preLoading = false;
    ReactDOM.render(
      <Provider store={store}>
        <Router history={browserHistory}>
          <Route path="/" component={App}>
            <IndexRoute component={HomePage}/>
            <Route path="/signup" component={SignUp}/>
            <Route path="/login" component={Login}/>
            <Route path="/logout" component={Logout}/>
            <Route path="/blog/edit(/:id)" component={BlogEditor}/>
            <Route path="/blog/:id" component={BlogPost}/>
            <Route path="*" component={HomePage}/>
          </Route>
        </Router>
      </Provider>,
      document.getElementById('root')
    );
  } else {
    browserHistory.push('/');
  }
});

function Logout() {
  firebase.auth().signOut().then(() => {
    store.dispatch(logoutUser());
    browserHistory.push('/');
  });
  return (
    <Alert bsStyle="info">Logged Out</Alert>
  );
}
